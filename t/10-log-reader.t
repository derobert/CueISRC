use Test::More;
use Test::Exception;
use FindBin;
use File::Next;
use Data::Dump qw(pp);
use strict;
use warnings qw(all);

$_ = 'xx-special-value';    # used to confirm its preserved
my $test_dir = 't-data/log-reader';

opendir my $dh, $test_dir
	or die "opendir t-data/log-reader: $!";
my @logs = map "$test_dir/$_", grep /\.log$/, readdir $dh;
closedir $dh;


# basic API
BEGIN { use_ok('App::CueISRC::LogReader') }

throws_ok { App::CueISRC::LogReader->new } qr/Missing required arguments: file/,
	'LogReader requires a file';
throws_ok { App::CueISRC::LogReader->new(file => '/nonexistent/file/name') }
	qr/Could not open/, 'LogReader notices nonexstent files';

# check one known good
my $reader = App::CueISRC::LogReader->new(
	file => 't-data/log-reader/morituri-Dvořák-1.log'
);
is($reader->cddb_discid, '5E10C006', 'got expected CDDB DiscID');
is(
	$reader->mb_discid,
	'rQ9RLSjpPl.IBhXlwOn2cBD1aQg-',
	'got expected MusicBrainz disc ID'
);

# confirm test files all load
foreach my $logpath (@logs) {
	$reader = App::CueISRC::LogReader->new(file => $logpath);
	isa_ok($reader, 'App::CueISRC::LogReader');
	can_ok($reader, 'cddb_discid', 'mb_discid');
	ok($reader->cddb_discid =~ /^[0-9a-fA-F]{8}$/, 'valid discid');
	ok($reader->mb_discid ne '', 'got MB discid')
}

is($_, 'xx-special-value', '$_ was preserved.');

plan tests => 3 + 2 + 4*@logs + 1;
