use Test::More;
use Test::Exception;
use File::Next;
use Data::Dump qw(pp);
use strict;
use warnings qw(all);

my $test_dir = 't-data/cue-reader';
$_ = 'xx-special-value';

opendir my $dh, $test_dir
	or die "opendir t-data/log-reader: $!";
my @cues = map "$test_dir/$_", grep /\.cue$/, readdir $dh;
closedir $dh;

# basic API
BEGIN { use_ok('App::CueISRC::CueReader') }

throws_ok { App::CueISRC::CueReader->new } qr/Missing required arguments: file/,
	'CueReader requires a file';
throws_ok { App::CueISRC::CueReader->new(file => '/nonexistent/file/name') }
	qr/Could not open/, 'CueReader notices nonexstent files';

# one with known results
my $reader = App::CueISRC::CueReader->new(file => 't-data/cue-reader/morituri-Dvořák-1.cue');
is($reader->cddb_discid, '5E10C006', 'got expected CDDB discid');
my $tracks = $reader->tracks;
is(scalar(@$tracks), 6, 'found expected number of tracks');

# and the known tracks
my @wanted = qw(
	NLA507000522   NLA507000523   NLA507000524
	NLA507000525   NLA506700368   NLA507100510
);
for (my $i = 0; $i < 6; ++$i) {
	my $tnum = 1 + $i;
	is($tracks->[$i]->number, $tnum, "Track $tnum: correct number");
	is($tracks->[$i]->isrc, $wanted[$i], "Track $tnum: correct ISRC");
}


# all the ones in the directory
my $total_track_count = 0;
foreach my $cuepath (@cues) {
	$reader = App::CueISRC::CueReader->new(file => $cuepath);
	isa_ok($reader, 'App::CueISRC::CueReader');
	can_ok($reader, qw(cddb_discid tracks) );
	ok($reader->cddb_discid =~ /^[0-9a-fA-F]{8}$/, 'valid discid');

	$tracks = $reader->tracks;
	isa_ok($tracks, 'ARRAY');
	$total_track_count += @$tracks;

	foreach my $track (@$tracks) {
		can_ok($track, qw(number isrc));
		ok($track->isrc =~ /^[A-Z]{2} [A-Z0-9]{3} [0-9]{7}$/x,
			'ISRC validates');
	}
}

is($_, 'xx-special-value', '$_ was preserved.');

plan tests => 3 + 2 + 2*6 + 4*@cues + 2*$total_track_count + 1;
