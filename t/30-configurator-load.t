use Test::More tests => 18;
use Test::Exception;
use Data::Dump qw(pp);
use strict;
use warnings 'all';
$_ = 'xx-special-value';    # used to confirm its preserved

BEGIN {
	use_ok('App::CueISRC::Configurator');
}

my $basic_config = {
	music       => {path     => 't-data/albums'},
	musicbrainz => {username => 'test', password => 'sekret'},
	worklog => {
		backend => {
			1 => {file => ':memory:', module => 'SQLite'},
		},
	},
};

# basic
my $cfg = new_ok('App::CueISRC::Configurator');
lives_ok { $cfg->load('t-data/config/00-basic') } 'load 00-basic';
is_deeply($cfg->_cfg, $basic_config, "Config parsed as expected");
isa_ok($cfg->mblite, 'App::CueISRC::MusicBrainzLite', 'Configurator->mblite');
isa_ok($cfg->worklog, 'App::CueISRC::Worklog', 'Configurator->worklog');

# explicit file name
lives_ok { $cfg->load('t-data/config/00-basic.conf') } 'load 00-basic.conf';
is_deeply($cfg->_cfg, $basic_config, "Config parsed as expected");

# multi-log
lives_ok { $cfg->load('t-data/config/01-multilog') } 'load 01-multilog';
isa_ok($cfg->worklog->_loggers->[0], 'App::CueISRC::Worklog::Hash', '1st logger');
isa_ok($cfg->worklog->_loggers->[1], 'App::CueISRC::Worklog::SQLite', '2nd logger');

# basic (in JSON)
lives_ok { $cfg->load('t-data/config/02-basic') } 'load 02-basic (JSON)';
is_deeply($cfg->_cfg, $basic_config, "Config parsed as expected");

# basic (in Perl)
lives_ok { $cfg->load('t-data/config/03-basic') } 'load 03-basic (Perl)';
is_deeply($cfg->_cfg, $basic_config, "Config parsed as expected");

# nonexistent
throws_ok { $cfg->load('t-data/config/zz-nonexistent') } qr/No configs/,
	'Errors on no configs';

# too many
throws_ok { $cfg->load('t-data/config/90-too-many') } qr/More than one/,
	'Errors on too many configs';


is($_, 'xx-special-value', '$_ was preserved.');
