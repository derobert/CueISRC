use 5.020;
use Test::More;
use Test::Exception;
use List::Util;
use File::Temp qw(tempdir);
use Data::Dump qw(pp);
my @backends;

BEGIN {
	$_ = 'xx-special-value';    # used to confirm its preserved
	@backends = (
		# have to assign in begin block because otherwise it'll be
		# assigned after the begin block.
		{
			name       => 'WL::Hash',
			persistent => 0,
			class      => 'App::CueISRC::Worklog::Hash',
			args       => [],
		},
		{
			name       => 'WL::SQLite (temporary)',
			persistent => 0,
			class      => 'App::CueISRC::Worklog::SQLite',
			args       => [file => ':memory:']
		},
		{
			name       => 'WL::SQLite (persistent)',
			persistent => 1,
			class      => 'App::CueISRC::Worklog::SQLite',
			args       => [file => tempdir(CLEANUP => 1) . '/test.db']
		},
	);

	# jessie's List::Util does not have uniqstr; work around... This
	# does something different (in particular, it always sorts) but
	# works for us.
	*uniqstr = List::Util->can("uniqstr") || sub {
		my %h; @h{@_} = undef; sort keys %h;
	};

	# everything uses the constants, and we need them too. Don't feel
	# like giving such a trivial module its own tests.
	use_ok('App::CueISRC::Worklog::Constants');
	
	# making this a subtest makes counting tests easier
	subtest 'Load all backends' => sub {
		# note: map localizes $_, so it's preserved.
		foreach my $class (uniqstr(sort map $_->{class}, @backends)) {
			use_ok($class);
		}
	};
}

my $cuefile     = 't-data/worklog/morituri-Dvořák-1.cue';
my @common_data = (
	program => 'App::CueISRC-TestSuite',
	version => '0.01',
	time    => 1474759191,
);
my @tests = ({
		name => 'cue sheet',
		type => 'cue',
		key  => [$cuefile],
		data => {@common_data},
	},
	{
		name => 'recording & isrc',
		type => 'rec_isrc',
		key  => ['4fb2c58c-b7e8-4109-af23-76198d656f90', 'NLA507000522'],
		data => {@common_data, cuefile => $cuefile},
	},
);

foreach my $backend (@backends) {
	subtest "Backend $backend->{name}" => sub {
		my $wl = new_ok($backend->{class}, $backend->{args}, '1st');
		foreach my $test (@tests) {
			my ($res, $dat) = $wl->get($test->{type}, $test->{key});
			is($res, WL_NOTFOUND, "Test $test->{name} not yet done");

			lives_ok { $wl->set($test->{type}, $test->{key}, $test->{data}) }
				"Test $test->{name} set done succeeds";

			($res, $dat) = $wl->get($test->{type}, $test->{key});
			is($res, WL_FOUND, "Test $test->{name} is now done");
			is_deeply($dat, $test->{data}, "Test $test->{name} data preserved.");
		}

		$wl = undef;    # clean up old;
		$wl = new_ok($backend->{class}, $backend->{args}, '2nd');

		foreach my $test (@tests) {
			my ($res, $dat) = $wl->get($test->{type}, $test->{key});
			if ($backend->{persistent}) {
				is($res, WL_FOUND, "Test $test->{name} is still done (persisted)");
			} else {
				is($res, WL_NOTFOUND,
					"Test $test->{name} is again not done (did not persist)");
			}
		}
	};
}

is($_, 'xx-special-value', '$_ was preserved.');
done_testing 3 + @backends;
