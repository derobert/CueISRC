use Test::More tests => 20;
use Test::Exception;
use Data::Dump qw(pp);


BEGIN {
	use_ok('App::CueISRC::Test::FakePrompter');
}

$_ = 'xx-special-value';    # used to confirm its preserved


my $fp = new_ok('App::CueISRC::Test::FakePrompter');

lives_ok { $fp->add('Exact String' => 'answer1') } 'Added exact string';
throws_ok { $fp->add('Exact String' => 'answer1') } qr/duplicate/,
	'Noticed conflicting exact string';
lives_ok { $fp->add(qr/regexp/ => 'answer2') } 'Added regexp';
lives_ok { $fp->add(qr/String/ => 'answer3') } 'Added 2nd regexp';
lives_ok { $fp->add('Tricky' => '0') } 'Added tricky string';

isa_ok($fp->prompter, 'CODE', 'obj->prompter');

is($fp->prompter->('Exact String'),
	'answer1', 'Exact string gave expected answer');
is($fp->prompter->('Some question matching the regexp given'),
	'answer2', 'Regexp gave expected answer');
is($fp->prompter->('Some Other String'),
	'answer3', 'Sometimes-overridden regexp gave expected answer');

throws_ok { $fp->prompter->('WTF prompt') } qr/unexpected/i,
	'Noticed unknown prompt';

is($fp->prompter->(-echo => '*', 'Exact String'),
	'answer1', '-echo works');
is($fp->prompter->(-default => 'foo', 'Exact String'),
	'answer1', '-default works');
is($fp->prompter->(-complete => 'whatever', 'Exact String'),
	'answer1', '-complete works');
is($fp->prompter->('-filenames', 'Exact String'),
	'answer1', '-filenames works');
is(
	$fp->prompter->(
		-complete => 'whatever',
		-echo     => '*',
		'-filenames', 'Exact String'
	),
	'answer1',
	'multiple arguments works'
);

ok($fp->prompter->('Tricky'), 'tricky answer is true');
is([ $fp->prompter->('Tricky') ]->[0], '0', 'works in list context');

is($_, 'xx-special-value', '$_ was preserved.');
