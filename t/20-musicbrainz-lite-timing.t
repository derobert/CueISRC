use Test::More tests => 8;
use Test::Exception;
use 5.020;
use strict;
use Data::Dump qw(pp);
use Time::HiRes qw(time);
$_ = 'xx-special-value';    # used to confirm its preserved

BEGIN {
	use_ok('App::CueISRC::MusicBrainzLite');
	use_ok('App::CueISRC::Test::FakeUserAgent');
}

my $fua = App::CueISRC::Test::FakeUserAgent->new;
my $mb = App::CueISRC::MusicBrainzLite->new(
	user_agent              => $fua,    # doesn't talk to MusicBrainz
	ratelimit_delay         => 0.5,
	ratelimit_retries       => 2,
	ratelimit_retry_backoff => 1.5,
	username                => 'foo',
	password                => 'bar',
);

# test time between two independent requests
$mb->get_release_data('587fb022-a44b-4a03-aa24-6c43bf8e8ca0') for (0..1);
my $time_diff = $fua->request_log->[1]{start} - $fua->request_log->[0]{end};
ok(abs($time_diff-0.5) <= 0.1, "2nd request within 0.1s of .5s: $time_diff");

# test retry backoff
$fua->clear_request_log;
push @{$fua->future_responses},
	({code => 503, message => '', headers => [], content => ''})x2;
diag("Please wait ~2.37 seconds.");
my $res = $mb->get_release_data('587fb022-a44b-4a03-aa24-6c43bf8e8ca0');

my $time_diff = $fua->request_log->[1]{start} - $fua->request_log->[0]{end};
ok(abs($time_diff-0.75) <= 0.1, "retry request within 0.1s of .75s: $time_diff");
my $time_diff = $fua->request_log->[2]{start} - $fua->request_log->[1]{end};
ok(abs($time_diff-1.125) <= 0.1, "retry request within 0.1s of 1.125s: $time_diff");
is($res->id, '587fb022-a44b-4a03-aa24-6c43bf8e8ca0', 'still got the release after retries');

# test running out of retries
$fua->clear_request_log;
push @{$fua->future_responses},
	({code => 503, message => '', headers => [], content => ''})x3;
diag("Please wait ~2.37 seconds.");
throws_ok { $mb->get_release_data('587fb022-a44b-4a03-aa24-6c43bf8e8ca0') } qr/Failed to query MB/, 'died on retries exceeded';

is($_, 'xx-special-value', '$_ was preserved.');
