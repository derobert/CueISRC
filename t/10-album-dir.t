use Test::More;
use Test::Exception;
use File::Next;
use Data::Dump qw(pp);
use strict;
use warnings qw(all);

my $test_dir = 't-data/albums';
$_ = 'xx-special-value';

opendir my $dh, $test_dir
	or die "opendir $test_dir: $!";
my @dirs = map "$test_dir/$_", grep !/^\./, readdir $dh;
closedir $dh;

# check basic API
BEGIN { use_ok('App::CueISRC::AlbumDir') }

throws_ok { App::CueISRC::AlbumDir->new } qr/Missing required arguments: directory/,
	'AlbumDir requires a directory';
throws_ok { 
	App::CueISRC::AlbumDir->new(directory => '/nonexistent/dir/name')->logfile
} qr/Could not open/, 'AlbumDir notices nonexstent directories';

# check one we know the data for
my $reader = App::CueISRC::AlbumDir->new(directory => 't-data/albums/morituri-dvořák-1');
is(
	$reader->logfile,
	'Antonín Dvořák; London Symphony Orchestra, Witold Rowicki - The Symphonies · Overtures (Disc 1 of 6).log',
	'got expected log'
);
is(
	$reader->cuefile,
	'Antonín Dvořák; London Symphony Orchestra, Witold Rowicki - The Symphonies · Overtures (Disc 1 of 6).cue',
	'got expected cue sheet file'
);
is(
	$reader->flacfile,
	'01. FAKE TRACK ONE AUDIO.flac',
	'got expected FLAC file'
);

# check all the dirs, yeah this will do the known good again...
foreach my $dir (@dirs) {
	$reader = App::CueISRC::AlbumDir->new(directory => $dir);
	isa_ok($reader, 'App::CueISRC::AlbumDir');
	can_ok($reader, qw(logfile cuefile flacfile) );
	ok($reader->logfile =~ /\.log$/, 'got a log file');
	ok($reader->cuefile =~ /\.cue$/, 'got a cue file');
	ok($reader->flacfile =~ /\.flac$/, 'got a flac file');
}

is($_, 'xx-special-value', '$_ was preserved.');

plan tests => 3 + 3 + 5*@dirs + 1;
