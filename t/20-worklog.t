use Test::More tests => 22;
use Test::Exception;
use Data::Dump qw(pp);
use strict;
use warnings 'all';
$_ = 'xx-special-value';    # used to confirm its preserved

# This presumes …Worklog::Hash works (there is a separate test script
# for that).

BEGIN {
	use_ok('App::CueISRC::Constants');
	use_ok('App::CueISRC::Worklog::Constants');
	use_ok('App::CueISRC::Worklog::Hash');
	use_ok('App::CueISRC::Worklog');
}

my $testcue = 't-data/worklog/morituri-Dvořák-1.cue';
my @rec_isrc = ('4fb2c58c-b7e8-4109-af23-76198d656f90', 'NLA507000522');

my $wlh1 = App::CueISRC::Worklog::Hash->new;
my $wlh2 = App::CueISRC::Worklog::Hash->new;

my $wl1 = new_ok('App::CueISRC::Worklog', [], 'C::Worklog 1');
my $wl2 = new_ok('App::CueISRC::Worklog', [], 'C::Worklog 2');

throws_ok { $wl1->get_cue($testcue) } qr/No.*log modules loaded/,
	'Worklog fails w/o any modules loaded';
lives_ok { $wl1->add_module($wlh1) } 'added hash no. 1 to 1st worklog';
lives_ok { $wl2->add_module($wlh2) } 'added hash no. 2 to 2nd worklog';
lives_and {
	is(($wl1->get_cue($testcue))[0],
		WL_NOTFOUND, 'Test file not yet done with only 1st hash');
};
lives_ok { $wl1->set_cue($testcue) }
	'Successfully set done through worklog';

my ($res, $dat) = $wl1->get_cue($testcue);
is($res, WL_FOUND, 'Test file is found now');
is($dat->{program}, CUEISRC_NAME, 'program as expected');
is($dat->{version}, CUEISRC_VERSION, 'version as expected');
ok(abs(time - $dat->{time}) <= 10, 'time as expected');    # 10s window

lives_and {
	is(($wl2->get_cue($testcue))[0],
		WL_NOTFOUND, 'Test file not yet done with only 2nd hash');
};
lives_ok { $wl2->add_module($wlh1) } 'added hash no. 1 to 2nd worklog';
lives_and {
	is(($wl2->get_cue($testcue))[0],
		WL_FOUND, 'Test file now done with 1st hash added, too');
};

lives_ok { $wl2->set_rec_isrc(@rec_isrc, $testcue) } 'set recording-isrc in 2nd worklog (hash 2 and 1)';
($res, $dat) = $wl1->get_rec_isrc(@rec_isrc);
is($res, WL_FOUND, 'and found recording-isrc in 1st worklog (hash 1)');
is($dat->{cuefile}, $testcue, 'and it has the correct cuefile');
is($_, 'xx-special-value', '$_ was preserved.');
