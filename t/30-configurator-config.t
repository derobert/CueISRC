use Test::More tests => 7;
use Test::Exception;
use Data::Dump qw(pp);
use App::CueISRC::Test::FakePrompter;
use strict;
use warnings 'all';
$_ = 'xx-special-value';    # used to confirm its preserved

BEGIN {
	use_ok('App::CueISRC::Configurator');
}

my $expected_config = {
	music       => {path     => 't-data/albums'},
	musicbrainz => {username => 'test', password => 'sekret'},
	worklog => {
		backend => {
			1 => {file => ':memory:', module => 'SQLite'},
		},
	},
};

my $fp = App::CueISRC::Test::FakePrompter->new;
$fp->add(q{Where is your music stored? } => 't-data/albums');
$fp->add(q{What's your MusicBrainz user name? } => 'test');
$fp->add(q{What's your MusicBrainz password? } => 'sekret');
$fp->add(qr/Where should the work log be stored/ => ':memory:');

my $cfg = new_ok('App::CueISRC::Configurator');
lives_ok { $cfg->prompt_config($fp->prompter) } '"prompt" for config';
is_deeply($cfg->_cfg, $expected_config, "Config generated as expected");
isa_ok($cfg->mblite, 'App::CueISRC::MusicBrainzLite', 'Configurator->mblite');
isa_ok($cfg->worklog, 'App::CueISRC::Worklog', 'Configurator->worklog');


is($_, 'xx-special-value', '$_ was preserved.');
