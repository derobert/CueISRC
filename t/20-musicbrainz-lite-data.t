use Test::More tests => 26;
use Test::Exception;
use 5.020;
use strict;
use Data::Dump qw(pp);
$_ = 'xx-special-value';    # used to confirm its preserved

BEGIN {
	use_ok('App::CueISRC::MusicBrainzLite');
	use_ok('App::CueISRC::Test::FakeUserAgent');
}

my $fua = App::CueISRC::Test::FakeUserAgent->new;
my $mb = App::CueISRC::MusicBrainzLite->new(
	user_agent      => $fua,
	ratelimit_delay => 0,      # FakeUserAgent doesn't talk to MusicBrainz
	username => 'testuser',
	password => 'testpass',
);

is($fua->auth_host, 'musicbrainz.org:443', 'correct authentication host');
is($fua->auth_realm, 'musicbrainz.org', 'correct authentication realm');
is($fua->auth_user, 'testuser', 'correct authentication username');
is($fua->auth_pass, 'testpass', 'correct authentication password');

my $rel;
lives_and { ok $rel = $mb->get_release_data('587fb022-a44b-4a03-aa24-6c43bf8e8ca0') } 'got 1st release';
is($rel->id, '587fb022-a44b-4a03-aa24-6c43bf8e8ca0', 'correct MBID');
is(
	$rel->title,
	q{Making Our Dreams Come True (Theme From TV Series "Laverne & Shirley")},
	'correct title'
);
is(scalar @{$rel->media}, 1, 'correct media count');

my $medium = $rel->media->[0];
is(scalar @{$medium->discids}, 0, 'correct discID count');
is(scalar @{$medium->tracks}, 2, 'correct track count');

my $track = $medium->tracks->[0];
is($track->number, 'A', 'A: correct track number');
is($track->title, q{Making Our Dreams Come True (Theme From TV Series "Laverne & Shirley")}, 'A: correct track name');
is($track->recording_id, '52b7f30f-cae8-43a2-9d94-03eaf9501702', 'A: correct track recording id');
is($track->recording_title, q{Making Our Dreams Come True}, 'A: correct track recording name');
is(scalar @{$track->recording_isrcs}, 1, 'A: correct recording ISRC count');
is($track->recording_isrcs->[0], 'ESA011624144', 'A: correct track recording ISRC');

# check 2nd track, which does not have a title (so should use recording
# title, apparently)
my $track = $medium->tracks->[1];
is($track->number, 'B', 'B: correct track number');
is($track->recording_title, q{Watching You}, 'B: correct recording name');
is($track->title, q{Watching You}, 'B: correct track name');

# this release has multiple discs and discids
lives_and { ok $rel = $mb->get_release_data('0015d5a0-8eb0-414f-b096-af885ec91559') } 'got 2nd release';
is(scalar @{$rel->media}, 6, '2nd: correct media count');

$medium = $rel->media->[0];
is(scalar @{$medium->discids}, 1, '2nd: correct discID count');
is($medium->discids->[0], 'rQ9RLSjpPl.IBhXlwOn2cBD1aQg-', '2nd: correct discID');

is($_, 'xx-special-value', '$_ was preserved.');
