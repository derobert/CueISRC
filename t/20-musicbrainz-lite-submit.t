use Test::More tests => 6;
use Test::Exception;
use Test::XML;
use 5.020;
use strict;
use Data::Dump qw(pp);
$_ = 'xx-special-value';    # used to confirm its preserved

BEGIN {
	use_ok('App::CueISRC::MusicBrainzLite');
	use_ok('App::CueISRC::Test::FakeUserAgent');
}

sub is_xml_safe {
	# is_xml clobbers $_. Bug reported; see
	# https://rt.cpan.org/Public/Bug/Display.html?id=118044
	local $_;
	is_xml(@_);
}

my $fua = App::CueISRC::Test::FakeUserAgent->new;
my $mb = App::CueISRC::MusicBrainzLite->new(
	user_agent      => $fua,
	ratelimit_delay => 0,       # FakeUserAgent doesn't talk to MusicBrainz
	username        => 'foo',
	password        => 'bar',
);

my $submission = {
	'4fb2c58c-b7e8-4109-af23-76198d656f90' => ['NLA507000522'],
	'ea910427-aa79-481f-87c5-a4752198c1f0' => ['NLA507000523'],
	'fakefake-fake-fake-fake-fakefakefake' =>
		['FAKE00000000', 'FAKE00000001'],
};

lives_and { ok($mb->submit_isrcs($submission)) } 'submitted ISRCs';

is(@{$fua->request_log}, 1, 'there is one logged request');

my $expectedXML = <<XML;
<?xml version="1.0" encoding="utf-8"?>
<metadata xmlns="http://musicbrainz.org/ns/mmd-2.0#">
  <recording-list>
    <recording id="4fb2c58c-b7e8-4109-af23-76198d656f90">
      <isrc-list count="1">
        <isrc id="NLA507000522"/>
      </isrc-list>
    </recording>
    <recording id="ea910427-aa79-481f-87c5-a4752198c1f0">
      <isrc-list count="1">
        <isrc id="NLA507000523"/>
      </isrc-list>
    </recording>
    <recording id="fakefake-fake-fake-fake-fakefakefake">
      <isrc-list count="2">
        <isrc id="FAKE00000000"/>
        <isrc id="FAKE00000001"/>
      </isrc-list>
    </recording>
  </recording-list>
</metadata>
XML

is_xml_safe($fua->request_log->[0]{content}, $expectedXML, 'generated expected XML');

is($_, 'xx-special-value', '$_ survived');
