use Test::More tests => 12;
use Test::Exception;
use Test::LongString;
use Data::Dump qw(pp);
use File::Slurper qw(read_text);
use File::Spec;
use File::Temp qw(tempdir);
use App::CueISRC::Test::FakePrompter;
use strict;
use warnings 'all';
$_ = 'xx-special-value';    # used to confirm its preserved

BEGIN {
	use_ok('App::CueISRC::Configurator');
}

my $parsed_config = {
	music       => {path     => 't-data/albums'},
	musicbrainz => {username => 'test', password => 'sekret'},
	worklog => {
		backend => {
			1 => {file => ':memory:', module => 'SQLite'},
		},
	},
};
my $expected_text = read_text('t-data/config/00-basic.conf');

my $cfg = new_ok('App::CueISRC::Configurator');
lives_ok { $cfg->_set_cfg($parsed_config) } 'forcefully injected configuration';
is_deeply($cfg->_cfg, $parsed_config, "injection worked");
isa_ok($cfg->mblite, 'App::CueISRC::MusicBrainzLite', 'Configurator->mblite');
isa_ok($cfg->worklog, 'App::CueISRC::Worklog', 'Configurator->worklog');

my $data;
open my $fh, '>', \$data or die "open data-fh failed; wtf: $!";
lives_ok { $cfg->save($fh) } 'obj->save(FH)';
close $fh or die "close data-fh failed: $!";

is_string($data, $expected_text, 'Expected config written');

my $dir = tempdir(CLEANUP => 1);
my $testfile = File::Spec->catfile($dir, 'foo.conf');
lives_ok { $cfg->save($testfile) } 'obj->save(filename)';
is((stat $testfile)[2] & 07777, 00600, 'file is secret as expected');
is_string(read_text($testfile), $expected_text, 'file content as expected');


is($_, 'xx-special-value', '$_ was preserved.');
