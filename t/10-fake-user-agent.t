use Test::More tests => 21;
use Test::Exception;
use HTTP::Request;
use Time::HiRes qw(clock_nanosleep CLOCK_MONOTONIC);
use Data::Dump qw(pp);


BEGIN {
	use_ok('App::CueISRC::Test::FakeUserAgent');
}

$_ = 'xx-special-value';    # used to confirm its preserved

my $url = q{https://musicbrainz.org/ws/2/release/587fb022-a44b-4a03-aa24-6c43bf8e8ca0?inc=recordings+media+discids+isrcs};

my $ua;
lives_ok { $ua = App::CueISRC::Test::FakeUserAgent->new } 'new FakeUserAgent';
is(scalar @{$ua->request_log}, 0, 'request log is empty');
ok($ua->request(HTTP::Request->new(GET => $url)), '1st request succeeded');
clock_nanosleep(CLOCK_MONOTONIC, 500_000_000, 0);
ok($ua->request(HTTP::Request->new(GET => $url)), '2nd request succeeded');
is(scalar @{$ua->request_log}, 2, 'request log has two entries');

my $time_diff = $ua->request_log->[1]{start} - $ua->request_log->[0]{end};
ok(abs($time_diff-0.5) <= 0.1, "within 0.1s of .5s: $time_diff");

lives_ok { $ua->clear_request_log } 'request log cleared';
is(scalar @{$ua->request_log}, 0, 'request log is again empty');
lives_ok {
	push @{$ua->future_responses},
		{
		code    => 503,
		message => 'unavailable',
		headers => [Foo => 'bar', Baz => 'taz'],
		content => 'no body'
		};
} 'added a future response';
my $res;
ok($res = $ua->request(HTTP::Request->new(GET => $url)), 'planned request succeeded');
is(scalar @{$ua->request_log}, 1, 'request log has one entry');
is($ua->request_log->[0]{code}, 503, 'request log entry 0 has code 503');

# actually test response is as expected
is($res->code, 503, 'response: correct status code');
is($res->message, 'unavailable', 'response: correct status message');
is($res->header('foo'), 'bar', 'response: correct Foo: header');
is($res->header('baz'), 'taz', 'response: correct Baz: header');
is($res->content, 'no body', 'response: correct body');

ok($res = $ua->request(HTTP::Request->new(GET => $url)), 'works after future responses run out');
is($res->code, 200, '... and gives real responses again');

is($_, 'xx-special-value', '$_ was preserved.');
