use Test::More tests => 20;
use Test::Exception;
use strict;
use warnings qw(all);
use Data::Dump qw(pp);

$_ = 'xx-special-value';    # used to confirm its preserved

# Note could do more testing (e.g., confirm we get expected values back
# in some places) but really we'd be testing Moo then. Not going to
# bother confirming Moo accessors work properly. The goal here is to
# test my code.
#
# This data is sort of one of the test albums, but shortened. The test
# album was picked because it happened to be a recent additio (at the
# bottom of http://musicbrainz.org/) at the time. Some of the data is
# made up and silly (e.g., vinly clearly doesn't have a discid).

# check we can load (no compile errors)
BEGIN { use_ok('App::CueISRC::MusicBrainzLite::Track') }
BEGIN { use_ok('App::CueISRC::MusicBrainzLite::Medium') }
BEGIN { use_ok('App::CueISRC::MusicBrainzLite::Release') }

# basic API, create some objects
my $track1;
lives_and {
	ok $track1 = App::CueISRC::MusicBrainzLite::Track->new(
		number          => 'A',
		title           => 'Making Our Dreams Come True',
		recording_id    => '52b7f30f-cae8-43a2-9d94-03eaf9501702',
		recording_title => 'Making Our Dreams Come True',
		recording_isrcs => ['ESA011624144'],
	);
} 'new Track A';

my $medium1;
lives_and { ok $medium1 = App::CueISRC::MusicBrainzLite::Medium->new }
          'new medium 1';

my $release;
lives_and {
	ok $release = App::CueISRC::MusicBrainzLite::Release->new(
		id    => '587fb022-a44b-4a03-aa24-6c43bf8e8ca0',
		title => 'Making Our Dreams Come True'
	);
} 'new release';

# let's try adding them.
my $res;
lives_ok { $res = $medium1->set_track(0, $track1) } 'set track 1';
ok($res == $track1, 'set_track returned the track');

lives_ok { $res = $release->set_medium(0, $medium1) } 'set medium 1';
ok($res == $medium1, 'set_medium returned the medium');

# let's try the factory versions
my $medium2;
lives_ok {
	$medium2 = $release->set_medium(1,
		discids => ['xxxxxxxxxxxxxxxxxxxxxxxxxxx-']);
} 'added new medium 2';
ok($medium2->isa('App::CueISRC::MusicBrainzLite::Medium'),
	q{and it's even a medium});

my $track2;
lives_ok {
	$track2 = $medium1->set_track(
		1,
		number          => 'B',
		recording_id    => '3e021247-2cc3-4c00-a98e-7ffb74cce923',
		recording_title => 'Watching You',
		recording_isrcs => ['ESA011624136'],
	);
} 'added new track 2';
ok($track2->isa('App::CueISRC::MusicBrainzLite::Track'), q{and it's even a track});

# amazingly, this release came with a CD. But for some reason they got
# the tracks in the wrong order. Guess you can't expect too much from
# CDs released in 1976.
lives_ok { $medium2->set_track(0, $track2) } 'set track 1 on medium 2';
lives_ok { $medium2->set_track(1, $track1) } 'set track 2 on medium 2';

# now some quick checks that it's all linked together correctly.
is($release->media->[0]->tracks->[0]->recording_isrcs->[0],
	'ESA011624144', 'medium one track one has expected ISRC');
is($release->media->[1]->tracks->[0]->recording_isrcs->[0],
	'ESA011624136', 'medium two track one has expected ISRC');
is($release->media->[1]->discids->[0],
	'xxxxxxxxxxxxxxxxxxxxxxxxxxx-', 'medium two has expected discid');

is($_, 'xx-special-value', '$_ was preserved.');
