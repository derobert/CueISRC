use 5.022;
use MusicBrainz::DiscID;
#  1      AUDIO   0      00:00:00(     0)     06:17:12( 28287)
#  2      AUDIO   0      06:17:12( 28287)     05:13:08( 23483)
#  3      AUDIO   0      11:30:20( 51770)     08:41:60( 39135)
#  4      AUDIO   0      20:12:05( 90905)     04:16:37( 19237)
#  5      AUDIO   0      24:28:42(110142)     10:17:38( 46313)
#  6      AUDIO   0      34:46:05(156455)     06:47:55( 30580)
#  7      AUDIO   0      41:33:60(187035)     02:58:52( 13402)
#  8      AUDIO   0      44:32:37(200437)     07:02:38( 31688)
# Leadout AUDIO   0      51:35:00(232125)
my @starts = qw(
     0
 28287
 51770
 90905
110142
156455
187035
200437
		);

@starts = qw(
    33
 73233
134808
174708
234333
274833
);

my $tot = 0;
foreach my $t (@starts) {
	$tot += $_ for split(//, int(($t+150)/75));
}
printf "%02X\n", $tot;

my $did = MusicBrainz::DiscID->new();
$did->put(1, map(150+$_, 274833+46800, @starts));
say $did->id;
