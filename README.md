Program to upload ISRC codes from .cue files to MusicBrainz

Current status of master branch:
[![build status](https://gitlab.com/derobert/CueISRC/badges/master/build.svg)](https://gitlab.com/derobert/CueISRC/commits/master)
[![coverage report](https://gitlab.com/derobert/CueISRC/badges/master/coverage.svg)](https://gitlab.com/derobert/CueISRC/commits/master)
[Coverage details](https://derobert.gitlab.io/CueISRC/coverage/coverage.html)
