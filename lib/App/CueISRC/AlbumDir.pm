package App::CueISRC::AlbumDir;
use 5.020;
use Moo;

has directory => (is => 'ro', required => 1);
has logfile => (is => 'lazy');
has cuefile => (is => 'lazy');
has flacfile => (is => 'lazy');

has _allfiles => (is => 'lazy');
has _dirh => (is => 'lazy');

sub BUILD {
	my ($self, $args) = @_;


	return;
}

sub _build_logfile { my $self = shift; $self->_build_x_file(log => qr/\.log$/) }
sub _build_cuefile { my $self = shift; $self->_build_x_file(cue => qr/\.cue$/) }
sub _build_flacfile {
	my $self = shift;
	$self->_build_x_file('track 1 FLAC' => qr/^01\. .* \.flac$/x)
}

sub _build_x_file {
	my ($self, $name, $pat) = @_;
	
	my @matches = grep /$pat/, @{$self->_allfiles};
	@matches > 1 and die "More than one $name file in ".$self->directory;

	return $matches[0]; # undef if none
}

sub _build__allfiles {
	my $self = shift;

	my $dirh = $self->_dirh;
	my @files = readdir $dirh
		or die "Could bot read directory @{[$self->directory]}: $!";

	# filter out . and ..
	return [ grep !/^\.\.?$/, @files ];
}

sub _build__dirh {
	my $self = shift;

	opendir my $dirh, $self->directory
		or die "Could not open @{[$self->directory]}: $!";

	return $dirh;
}


1;
