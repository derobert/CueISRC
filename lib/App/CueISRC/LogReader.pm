package App::CueISRC::LogReader;
use 5.020;
use Moo;
use Fcntl qw(SEEK_SET);

has file => (is => 'ro', required => 1);
has cddb_discid => (is => 'rwp');
has mb_discid => (is => 'rwp');

has _fh => (is => 'rw');

sub BUILD {
	my ($self, $args) = @_;

	open my $fh, '<', $args->{file}
		or die "Could not open $args->{file}: $!";
	$self->_fh($fh);

	$self->_parse;

	return;
}

sub _parse {
	my ($self) = @_;

	my $fh = $self->_fh;
	seek($fh, 0, SEEK_SET); # just in case
	<$fh> =~ /^Logfile created by: morituri/
		or die "Not a morituri log";

	while(defined(my $line = <$fh>)) {
		$line =~ /^CDDB disc id:\s+([a-fA-F0-9]+)$/
			and $self->_set_cddb_discid(uc $1);
		$line =~ /^MusicBrainz disc id:\s+(\S+)$/
			and $self->_set_mb_discid($1);
	}
}

1;
