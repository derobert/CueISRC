package App::CueISRC::Constants;
use Exporter qw(import);
use 5.020;
our @EXPORT = qw(CUEISRC_NAME CUEISRC_VERSION CUEISRC_URL);
our @EXPORT_OK = qw(CUEISRC_DIR);

use constant {
	CUEISRC_NAME       => 'CueISRC',
	CUEISRC_VERSION    => '0.01',
	CUEISRC_URL        => 'https://gitlab.com/derobert/CueISRC',
	CUEISRC_DIR        => 'CueISRC',
};
