package App::CueISRC::Worklog::Constants;
use Exporter qw(import);
use 5.020;

# this feels so un-Perlish, but it works... and shouldn't escape from
# the Worklog classes anyway.
use enum qw(:WL_ NOTFOUND=1 FOUND UNSUPPORTED);
our @EXPORT = qw(WL_NOTFOUND WL_FOUND WL_UNSUPPORTED);

1;
