package App::CueISRC::Worklog::Hash;
use 5.020;
use Moo;
use App::CueISRC::Worklog::Constants qw(WL_NOTFOUND WL_FOUND);

has _data => (is => 'ro', default => sub { +{} } );

sub set {
	my ($self, $type, $keys, $attr) = @_;
	$self->_data->{$type}{$self->_realkey($keys)} = $attr;
	return;
}

sub get {
	my ($self, $type, $keys) = @_;
	wantarray or die "test demands array context";

	my $realkey = $self->_realkey($keys);
	exists $self->_data->{$type}{$realkey} 
		? (WL_FOUND, $self->_data->{$type}{$realkey})
		: (WL_NOTFOUND, undef);
}

sub _realkey {
	my ($self, $keys) = @_;
	# splat is just to make it visually easier to debug
	return join('*', map length($_).":$_", @$keys);
}

1;
