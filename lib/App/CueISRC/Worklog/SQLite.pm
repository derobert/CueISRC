package App::CueISRC::Worklog::SQLite;
use 5.020;
use Moo;
use DBI;
use SQL::Abstract;    # overkill, but whatever
use App::CueISRC::Worklog::Constants;
use Data::Dump qw(pp);

# Goal: Avoid writing an ORM. Would use DBIC (etc.) if I wanted that.

has file => (is => 'ro', required => 1);

has _dbh => (is => 'lazy');
has _sql => (is => 'lazy');

has _types => (is => 'lazy', init_arg => undef);

sub get {
	my ($self, $type, $keyvals) = @_;
	wantarray or die "test demands array context";

	my $ti = $self->_types->{$type}
		or die "Unknown type: $type";

	my %where;
	@where{@{$ti->{key}}} = @$keyvals;
	my ($sql, @bind) = $self->_sql->select(
		$ti->{table}, [keys %{$ti->{data_rev}}], \%where);
	my $sth = $self->_dbh->prepare_cached($sql);
	$sth->execute(@bind);

	my @res = $sth->fetchrow_array;
	$sth->finish;

	@res or return (WL_NOTFOUND, undef);
	my %res;
	@res{values %{$ti->{data_rev}}} = @res;
	return (WL_FOUND, \%res);
}

sub set {
	my ($self, $type, $keys, $attr) = @_;

	my $ti = $self->_types->{$type}
		or die "Unknown type: $type";

	my %data;
	@data{@{$ti->{data_fwd}}{keys %$attr}} = values %$attr;
	@data{@{$ti->{key}}} = @$keys;
	
	my ($sql, @bind) = $self->_sql->insert($ti->{table}, \%data);
	my $sth = $self->_dbh->prepare_cached($sql);
	$sth->execute(@bind);
	$sth->finish;

	return;
}

sub _build__dbh {
	my $self = shift;

	my $dbh = DBI->connect('dbi:SQLite:dbname=' . $self->file, '', '', {
		AutoCommit       => 1,
		FetchHashKeyName => 'NAME_lc',
		PrintError       => 0,
		RaiseError       => 1,
	});

	my ($vers) = $dbh->selectrow_array('PRAGMA user_version');
	if (0 == $vers) {
		$dbh->do(<<CREATE);
CREATE TABLE cuesheets (
  path      TEXT      NOT NULL PRIMARY KEY,
  program   TEXT      NOT NULL,
  version   TEXT      NOT NULL,
  done_at   INTEGER   NOT NULL
)
CREATE
		$dbh->do(<<CREATE);
CREATE TABLE recording_isrcs (
  recording_mbid   TEXT      NOT NULL,
  isrc             TEXT      NOT NULL,
  program          TEXT      NOT NULL,
  version          TEXT      NOT NULL,
  submitted_at     INTEGER   NOT NULL,
  cuesheet         TEXT      NOT NULL,
  PRIMARY KEY(recording_mbid, isrc)
)
CREATE
		$dbh->do(q{PRAGMA user_version = 1});
	} elsif (1 == $vers) {
		# current, do nothing
	} else {
		die "@{[$self->file]} is future database version $vers (current=1)";
	}

	return $dbh;
}

sub _build__types {
	my $self = shift;
	my $h = {
		cue => {
			table    => 'cuesheets',
			key      => [qw(path)],
			data_fwd => {
				program => 'program',
				version => 'version',
				time    => 'done_at',
			},
		},
		rec_isrc => {
			table    => 'recording_isrcs',
			key      => [qw(recording_mbid isrc)],
			data_fwd => {
				program => 'program',
				version => 'version',
				time    => 'submitted_at',
				cuefile => 'cuesheet',
			},
		},
	};

	return $self->_add_data_rev($h);
}

sub _add_data_rev {
	# this MODIFIES type_info.
	my ($self, $type_info) = @_;

	while (my ($type, $info) = each %$type_info) {
		# keys and values return in the same order, so this works
		@{$info->{data_rev}}{values %{$info->{data_fwd}}} = keys %{$info->{data_fwd}};
	}

	return $type_info;
}

sub _build__sql {
	SQL::Abstract->new();
}

1;
