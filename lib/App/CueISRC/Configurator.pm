package App::CueISRC::Configurator;
use 5.020;
use Moo;
use Config::Any;
use Module::Load qw();    # do not import, conflicts with our load
use Fcntl qw(O_WRONLY O_CREAT O_TRUNC);
use File::BaseDir;
use File::Spec;
use File::Path qw();
use App::CueISRC::Worklog;
use App::CueISRC::MusicBrainzLite;
use App::CueISRC::Constants qw(CUEISRC_DIR);
use Data::Dump qw(pp);

has config_file => (is => 'rwp', init_arg => undef);
has config_dir => (is => 'lazy', init_arg => 'config_dir');
has data_dir   => (is => 'lazy', init_arg => 'data_dir');

has worklog => (is => 'lazy', clearer => '_clear_worklog', init_arg => undef);
has mblite => (is => 'lazy',  clearer => '_clear_mblite',  init_arg => undef);

has _cfg =>
	(is => 'rw', writer => '_set_cfg', init_arg => undef, trigger => 1);
has _basedir => (
	is       => 'ro',
	init_arg => 'basedir',
	default  => sub { File::BaseDir->new },
);

sub load {
	my $self = shift;
	my @paths = @_;

	# annoying Config::Any API.
	my ($method, @find_what);
	if (1 == @paths && -e $paths[0]) {
		# single file which exists = not a stem
		$method = 'load_files';
		@find_what = (files => \@paths);
	} else {
		# multiple, or not exists = stems
		$method = 'load_stems';
		@find_what = (stems => \@paths);
	}

	my $cfgs = Config::Any->$method({
		@find_what,
		use_ext     => 1,
		driver_args => {
			General => {
				-LowerCaseNames   => 1,
				-UseApacheInclude => 1,
				-InterPolateVars  => 1,
				-InterPolateEnv   => 1,
			},
		},
	});

	@$cfgs == 0 and die "No configs?";
	@$cfgs != 1 and die "More than one config not supported";

	$self->_set_config_file((keys %{$cfgs->[0]})[0]);
	$self->_set_cfg((values %{$cfgs->[0]})[0]);

	return;
}

sub prompt_config {
	my ($self, $prompter) = @_;
	my %cfg;

	# Note: the weirdly quoted things like "$res" are to turn
	# IO::Prompter's Contextual::Return objects into normal strings.
	# Contextual::Return, for example, causes Data::Dump to OOM. Best
	# not to propogate it far...

	my $res = $prompter->(
		-filenames => -complete => 'dirnames',
		'Where is your music stored? '
	);
	$res or die "Can't configure w/o an album path";
	$cfg{music}{path} = "$res";

	$res = $prompter->(q{What's your MusicBrainz user name? });
	$res or die "Can't configure w/o a user name";
	$cfg{musicbrainz}{username} = "$res";

	$res = $prompter->(-echo => '*', q{What's your MusicBrainz password? });
	$res or die "Can't configure w/o a password";
	$cfg{musicbrainz}{password} = "$res";

	my $def = File::Spec->catfile($self->data_dir, 'worklog.sqlite');
	$res = $prompter->(
		-default => $def,
		qq{Where should the work log be stored? [default=$def] }
	);
	$res or die "Can't configure w/o a place to store the database.";
	$cfg{worklog}{backend}{1} = {
		file => "$res",
		module => 'SQLite',
	};

	$self->_set_cfg(\%cfg);

	return;
}

sub save { 
	my ($self, $destination) = @_;
	$destination //= File::Spec->catfile($self->config_dir, 'main.conf');

	# so much easier not to write $self->_cfg all over the place,
	# especially with the stunts needed to interpolate it.
	my $cfg = $self->_cfg or die "Tried to save without a config";

	# we don't use Config::General's save_string/save_file because that
	# doesn't give us enough control over the output.

	my $fh;
	if ($destination->DOES('GLOB') || $destination->can('print')) {
		# file handles can't print, until you try, which autoloads it.
		# So need to test for glob as well.
		$fh = $destination;
	} else {
		sysopen($fh, $destination, O_WRONLY|O_CREAT|O_TRUNC, 0600)
			or die "Could not open $destination: $!";
		$self->_set_config_file($destination);
	}

	$fh->print(<<CONF);
<Music>
	Path $cfg->{music}{path}
</Music>

<MusicBrainz>
	Username $cfg->{musicbrainz}{username}
	Password $cfg->{musicbrainz}{password}
</MusicBrainz>

<Worklog>
CONF

	foreach my $b_key (sort keys %{$cfg->{worklog}{backend}}) {
		my $dat = $cfg->{worklog}{backend}{$b_key};
		$fh->print(qq{\t<Backend $b_key>\n});
		$fh->print(qq{\t\tModule $dat->{module}\n});
		foreach my $param (sort keys %$dat) {
			next if $param eq 'module';
			$fh->print(qq{\t\t\u$param $dat->{$param}\n});
		}
		$fh->print(qq{\t</Backend>\n});
	}

	$fh->print("</Worklog>\n");

	return;
}

sub _build_config_dir {
	my $self = shift;

	my $dir = $self->_basedir->config_dirs(CUEISRC_DIR)
		// $self->_basedir->config_home(CUEISRC_DIR)
		// die("Could not figure out where my config belongs");

	-d $dir
		|| File::Path::make_path($dir)
		|| die("Could not create $dir: $!");

	return $dir;
}

sub _build_data_dir {
	my $self = shift;

	my $dir = $self->_basedir->data_dirs(CUEISRC_DIR)
		// $self->_basedir->data_home(CUEISRC_DIR)
		// die("Could not figure out where my data belongs");

	-d $dir
		|| File::Path::make_path($dir)
		|| die("Could not create $dir: $!");

	return $dir;
}

sub _build_mblite {
	my $self = shift;
	my $cfg = $self->_cfg->{musicbrainz}
		or die "No MusicBrainz config found";

	App::CueISRC::MusicBrainzLite->new($cfg);
}

sub _build_worklog {
	my $self = shift;
	my $cfg = $self->_cfg->{worklog}
		or die "No MusicBrainz config found";
	my $backends = $cfg->{backend}
		or die "No backends in config";
	
	my $worklog = App::CueISRC::Worklog->new;
	my @backend_order = sort { $a cmp $b } keys %$backends;
	foreach my $backend (@backend_order) {
		my %args = %{$backends->{$backend}};
		my $module = 'App::CueISRC::Worklog::' . delete($args{module});
		Module::Load::load $module;
		$worklog->add_module($module->new(\%args));
	}

	return $worklog;
}

sub _trigger__cfg {
	my $self = shift;

	$self->_clear_worklog;
	$self->_clear_mblite;
}

1;
