package App::CueISRC::MusicBrainzLite;
use 5.020;
use Moo;
use LWP::UserAgent;
use XML::LibXML;
use Time::HiRes
	qw(clock_gettime clock_nanosleep CLOCK_MONOTONIC TIMER_ABSTIME);
use URI::Escape qw(uri_escape);
use App::CueISRC::Constants;
use App::CueISRC::MusicBrainzLite::Release;
use App::CueISRC::MusicBrainzLite::Track;
use constant _MB_NAMESPACE => 'http://musicbrainz.org/ns/mmd-2.0#';

# These you must set
has _ws_auth_user => (is => 'ro', init_arg => 'username', required => 1);
has _ws_auth_pass => (is => 'ro', init_arg => 'password', required => 1);

# The rest is optional.
has _user_agent =>
	(is => 'lazy', init_arg => 'user_agent', required => 1, trigger => 0);
has _client => (
	is       => 'ro',
	init_arg => 'client_string',
	required => 1,
	default  => CUEISRC_NAME . '-' . CUEISRC_VERSION
);

# these you only change for testing (e.g., with FakeUserAgent) or for
# using your own server
has _ws_url => (
	is       => 'ro',
	init_arg => 'ws_url',
	required => 1,
	default  => 'https://musicbrainz.org/ws/2/',
);
has _ws_auth_host => (
	is       => 'ro',
	init_arg => 'ws_host',
	requored => 1,
	default  => 'musicbrainz.org:443'
);
has _ws_auth_realm => (
	is       => 'ro',
	init_arg => 'ws_realm',
	requored => 1,
	default  => 'musicbrainz.org'
);
has _rl_delay => (
	is       => 'ro',
	init_arg => 'ratelimit_delay',
	required => 1,
	default  => 2,
);

# you can tune the backoff, but probably no need (unless your network
# has issues)
has _rl_backoff => (
	is       => 'ro',
	init_arg => 'ratelimit_retry_backoff',
	required => 1,
	default  => 2,
);
has _rl_backoff_max => (
	is => 'ro',
	init_arg => 'ratelimit_retry_backoff_max',
	required => 1,
	default => 256,
);
has _rl_retries => (
	is => 'ro',
	init_arg => 'ratelimit_retries',
	required => 1,
	default => 13, # around 34 minutes total, given defaults above
);

# change for forks
has _agent_prefix => (
	is       => 'ro',
	default  => sub { (__PACKAGE__ =~ s/::/-/r).'/'.CUEISRC_VERSION },
);
has _agent_suffix => (
	is => 'ro',
	default => " ( @{[CUEISRC_URL]} )",
);

# shouldn't need to change these
has _parser => (is => 'lazy');
has _last_req => (is => 'rw', default => 0);

sub BUILD {
	my ($self, $args) = @_;

	$self->_user_agent->credentials(
		$self->_ws_auth_host, $self->_ws_auth_realm,
		$self->_ws_auth_user, $self->_ws_auth_pass
	);

	return;
}

sub get_release_data {
	my ($self, $mbid) = @_;
	local $_;

	my $url = $self->_ws_url
		. "release/${mbid}?inc=recordings+media+discids+isrcs";
	my $res = $self->_mb_get($url);
	$res->is_success or die "Failed to query MB";

	# oh joys, XML.
	my $doc = $self->_parser->load_xml(string => $res->content);

	# rule one of XML: simple things should be hard. The MB XML result
	# has a namespace. So we have to register it, or else we can't find
	# nodes easily. (Not that we care about the namespace at all! So
	# tempting to use the beta JSON interface instead!)
	my $xpc  = XML::LibXML::XPathContext->new($doc);
	$xpc->registerNs(mb => _MB_NAMESPACE);
	

	my @nodes = $xpc->findnodes('/mb:metadata/mb:release');
	1 == @nodes or die "Did not get one and only one release";
	my $n_release = $nodes[0];
	my $release = App::CueISRC::MusicBrainzLite::Release->new(
		id => $n_release->attributes->getNamedItem('id')->value, # see rule 1
		title => $xpc->findvalue('./mb:title', $n_release)
	);

	my @n_media = $xpc->findnodes('./mb:medium-list/mb:medium', $n_release);
	foreach my $n_medium (@n_media) {
		
		my $medium = $release->set_medium(
			$xpc->findvalue('./mb:position', $n_medium) - 1,
			discids => [
				map($_->attributes->getNamedItem('id')->value,
					$xpc->findnodes('./mb:disc-list/mb:disc', $n_medium))
			],
		);

		my @n_tracks = $xpc->findnodes('./mb:track-list/mb:track', $n_medium);
		foreach my $n_track (@n_tracks) {
			my @n_recordings = $xpc->findnodes('./mb:recording', $n_track);
			1 == @n_recordings or die "More than one recording on a track?";
			my $n_recording = $n_recordings[0];

			# not all tracks have a <title> element, apparently you just
			# use the recording title then. UGH.
			my $n_titles = $xpc->findnodes('./mb:title', $n_track);
			my @titleinfo;
			if (1 == $n_titles->size) {
				@titleinfo = (title => $n_titles->to_literal);
			} elsif (0 == $n_titles->size) {
				# leavy empty
			} else {
				die "More than one title in track.";
			}

			$medium->set_track(
				$xpc->findvalue('./mb:position', $n_track) - 1,
				number => $xpc->findvalue('./mb:number', $n_track),
				@titleinfo,
				recording_id => $n_recording->attributes->getNamedItem('id')->value,
				recording_title => $xpc->findvalue('./mb:title', $n_recording),
				recording_isrcs => [
					map($_->attributes->getNamedItem('id')->value,
						$xpc->findnodes('./mb:isrc-list/mb:isrc', $n_recording))
				],
			);
		}
	}

	return $release;
}

sub submit_isrcs {
	my ($self, $isrc_data) = @_;

	my $url
		= $self->_ws_url . 'recording/?client=' . uri_escape($self->_client);
	my $doc = XML::LibXML::Document->new('1.0', 'utf-8');
	my $root = $doc->createElementNS(_MB_NAMESPACE, 'metadata');
	my $reclist = $doc->createElementNS(_MB_NAMESPACE, 'recording-list');
	$root->appendChild($reclist);

	# go in sorted order because it makes the test suite easier
	foreach my $recid (sort keys %$isrc_data) {
		my $isrcs = $isrc_data->{$recid};
		my $rec = $doc->createElementNS(_MB_NAMESPACE, 'recording');
		$rec->setAttribute(id => $recid);
		$reclist->appendChild($rec);

		# no idea why ISRC lists have a count attribute when other lists
		# don't—but that's what
		# http://wiki.musicbrainz.org/Development/XML_Web_Service/Version_2#ISRC_submission
		# shows
		my $isrclist = $doc->createElementNS(_MB_NAMESPACE, 'isrc-list');
		$isrclist->setAttribute(count => scalar(@$isrcs));
		$rec->appendChild($isrclist);

		for my $isrc (@$isrcs) {
			my $n_isrc = $doc->createElementNS(_MB_NAMESPACE, 'isrc');
			$n_isrc->setAttribute(id => $isrc);
			$isrclist->appendChild($n_isrc);
		}
	}

	$doc->setDocumentElement($root);

	my $res = $self->_mb_post($url, $doc->toString);
	return 200 == $res->code;
}

sub _mb_get {
	my ($self, $url) = @_;

	$self->_mb_request(HTTP::Request->new(GET => $url));
}

sub _mb_post {
	my ($self, $url, $xml) = @_;

	$self->_mb_request(
		HTTP::Request->new(
			POST => $url,
			['Content-type' => 'text/xml; charset=UTF-8'], $xml
		));
}

sub _mb_request {
	my ($self, $http_request) = @_;

	# yeah for being a single-threaded non-async daemon, can just sleep
	# here to stay under the rate limit
	my $next = $self->_last_req + $self->_rl_delay;
	clock_nanosleep(CLOCK_MONOTONIC, 1_000_000_000*$next, TIMER_ABSTIME);

	my $res;
	my $backoff_delay = $self->_rl_delay;
	for (my $retry_no = 0; ; ++$retry_no) {
		$res = $self->_user_agent->request($http_request);
		unless (
			(503 == $res->code)
			|| (500 == $res->code
				&& $res->header('Client-Warning') eq 'Internal response'))
		{
			last;
		}
		last if $retry_no == $self->_rl_retries;

		$backoff_delay *= $self->_rl_backoff;
		$backoff_delay > $self->_rl_backoff_max
			and $backoff_delay = $self->_rl_backoff_max;
		clock_nanosleep(CLOCK_MONOTONIC, 1_000_000_000*$backoff_delay, 0);
	}

	$self->_last_req(clock_gettime(CLOCK_MONOTONIC));
	return $res;
}

sub _build__user_agent {
	my $self = shift;

	my $ua = LWP::UserAgent->new(
		ssl_opts => { verify_hostname => 1 },
		parse_head => 0, # any HTML responses are errors
		protocols_allowed => [ 'https' ],
		max_redirect => 1, # re-trying for auth counts as one
	);

	# can't pass to new bacause want libwww-perl/VERS in the middle.
	$ua->agent($self->_agent_prefix . $ua->_agent . $self->_agent_suffix);

	return $ua;
}

sub _build__parser {
	my $self = shift;

	XML::LibXML->new(
		recover => 0,
		no_network => 1,
	);
}

1;
