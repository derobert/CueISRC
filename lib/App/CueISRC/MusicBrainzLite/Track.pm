package App::CueISRC::MusicBrainzLite::Track;
use 5.020;
use Moo;

# This is minimal, only concerned with what we reasonably need.
has number => (is => 'ro', required => 0);
has title => (
	is       => 'ro',
	required => 1,
	lazy     => 1,
	default  => sub { $_[0]->recording_title },
);
has [qw(recording_id recording_title)] => (is => 'ro', required => 0);
has recording_isrcs => (is => 'ro', default => sub { [] });

1;
