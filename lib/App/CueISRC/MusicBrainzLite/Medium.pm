package App::CueISRC::MusicBrainzLite::Medium;
use 5.020;
use Moo;
use App::CueISRC::MusicBrainzLite::Track;
use Scalar::Util qw(blessed);

# This is minimal, only concerned with what we reasonably need.
has discids => (is => 'ro', required => 1, default => sub { [] });
has tracks =>
	(is => 'ro', required => 1, init_arg => undef, default => sub { [] });
1;

sub set_track {
	my $self = shift;
	my $position = shift;
	my $track;
	if (blessed $_[0] && $_[0]->DOES('App::CueISRC::MusicBrainzLite::Track')) {
		$track = $_[0]
	} else {
		$track = App::CueISRC::MusicBrainzLite::Track->new(@_);
	}
	$self->tracks->[$position] = $track;
	return $track;
}

1;
