package App::CueISRC::MusicBrainzLite::Release;
use 5.020;
use Moo;
use Scalar::Util qw(blessed);
use App::CueISRC::MusicBrainzLite::Medium;

# This is minimal, only concerned with what we reasonably need.
has [qw(id title)] => ( is => 'ro', required => 1 );
has media => (is => 'ro', init_arg => undef, default => sub { [] });

sub set_medium {
	my $self = shift;
	my $position = shift;
	my $medium;
	if (blessed $_[0] && $_[0]->DOES('App::CueISRC::MusicBrainzLite::Medium')) {
		$medium = $_[0]
	} else {
		$medium = App::CueISRC::MusicBrainzLite::Medium->new(@_);
	}
	$self->media->[$position] = $medium;
	return $medium;
}

1;
