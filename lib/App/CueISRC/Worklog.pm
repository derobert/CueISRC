package App::CueISRC::Worklog;
use 5.020;
use Moo;
use DBI;
use App::CueISRC::Constants;
use App::CueISRC::Worklog::Constants;

has _loggers => ( is => 'ro', default => sub { [ ] } );

sub add_module {
	my ($self, $obj) = @_;

	push @{$self->_loggers}, $obj;
	return;
}

sub get_cue {
	my ($self, $cuefile) = @_;
	$self->_get(cue => [ $cuefile ]);
}
sub set_cue {
	my ($self, $cuefile) = @_;
	$self->_set(cue => [ $cuefile ], {});
}

sub get_rec_isrc {
	my ($self, $rec_mbid, $isrc) = @_;
	$self->_get(rec_isrc => [ $rec_mbid, $isrc ]);
}
sub set_rec_isrc {
	my ($self, $rec_mbid, $isrc, $cuefile) = @_;
	$self->_set(rec_isrc => [ $rec_mbid, $isrc ], { cuefile => $cuefile });
}

sub _get {
	my ($self, $type, $keys) = @_;
	@{$self->_loggers}
		or die "No work done log modules loaded; get_* called";
	foreach my $logger (@{$self->_loggers}) {
		my ($res, $dat) = $logger->get($type, $keys);
		(WL_FOUND == $res) and return ($res, $dat);
	}
	return (WL_NOTFOUND, undef);
}

sub _set {
	my ($self, $type, $keys, $args) = @_;
	
	# easiest to shallow copy & modify
	$args = {%$args};
	$args->{program} //= CUEISRC_NAME;
	$args->{version} //= CUEISRC_VERSION;
	$args->{time}    //= time;

	@{$self->_loggers}
		or die "No work done log modules loaded; set_* called";
	foreach my $logger (@{$self->_loggers}) {
		$logger->set($type, $keys, $args);
	}
	return;
}

1;
