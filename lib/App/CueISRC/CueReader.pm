package App::CueISRC::CueReader::Track {
	use 5.020;
	use Moo;
	use Fcntl;

	has number => (is => 'ro');
	has isrc => (is => 'ro');
}

package App::CueISRC::CueReader {
	use 5.020;
	use Moo;
	use Fcntl qw(SEEK_SET);

	has file => (is => 'ro', required => 1);
	has cddb_discid => (is => 'rwp');
	has tracks => (is => 'rwp');

	has _fh => (is => 'rw');

	sub BUILD {
		my ($self, $args) = @_;

		open my $fh, '<', $args->{file}
			or die "Could not open $args->{file}: $!";
		$self->_fh($fh);

		$self->_parse;

		return;
	}

	sub _parse {
		my ($self) = @_;

		my $fh = $self->_fh;
		seek($fh, 0, SEEK_SET); # just in case

		my $tnum = undef;
		my @tracks;
		while(defined(my $line = <$fh>)) {
			$line =~ /^REM DISCID ([a-fA-F0-9]+)$/
				and $self->_set_cddb_discid(uc $1);
			$line =~ /^\s*TRACK ([0-9]+) AUDIO$/
				and $tnum = 0 + $1;
			if ($line =~ /^\s*ISRC "?([-A-Za-z0-9]+)"?$/) {
				(my $isrc = $1) =~ y/-//d;
				defined $tnum or die "ISRC $1 w/o a track number";
				defined $tracks[$tnum-1] and die "second ISRC for track $tnum";
				$tracks[$tnum-1] = App::CueISRC::CueReader::Track->new(
					number => $tnum,
					isrc => uc($isrc)
				);
			}
		}
		$self->_set_tracks(\@tracks);
		return;
	}

}

1;
