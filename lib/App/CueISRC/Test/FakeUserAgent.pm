package App::CueISRC::Test::FakeUserAgent;
use 5.020;
use Moo;
use HTTP::Response;
use HTTP::Headers;
use File::Slurper qw(read_lines read_binary);
use Time::HiRes qw(clock_gettime CLOCK_MONOTONIC);
use Data::Dump qw(pp);

has basedir    => (is => 'ro', required => 1, default => 't-data/fake-ua');
has fakescheme => (is => 'ro', required => 1, default => 'https');
has fakehost   => (is => 'ro', required => 1, default => 'musicbrainz.org');

has request_log => (is => 'rwp', init_arg => undef, default => sub { [] });
has future_responses => (is => 'rw', default => => sub { [] } );

has [qw(auth_user auth_pass auth_host auth_realm)] => (is => 'rwp');

sub request {
	my ($self, $request) = @_;
	my $start_time = clock_gettime(CLOCK_MONOTONIC);

	$request->uri->scheme eq $self->fakescheme
		or die "Unexepected scheme @{[$request->uri->scheme]}";
	$request->uri->host eq $self->fakehost
		or die "Unexepected host @{[$request->uri->host]}";

	my $res;
	if (my $plan = shift @{$self->future_responses}) {
		$res = HTTP::Response->new($plan->{code}, $plan->{message},
			$plan->{headers}, $plan->{content});
	} else {
		my $fn_base
			= $self->basedir . $request->uri->path . '?' . $request->uri->query;

		# the following is a good-enough HTTP header parser. Please don't
		# put to real use.
		my @hdr = read_lines("$fn_base.header", 'US-ASCII', 1, 0);
		my ($httpver, $code, $msg) = split(/\s+/, shift @hdr, 3);

		my $headers = HTTP::Headers->new(
			map(split(': ', $_, 2), @hdr)
		);

		$res = HTTP::Response->new($code, $msg, $headers, read_binary("$fn_base.body"));
	}
	$res->request($request);

	# log it for ratelimit testing
	push @{$self->request_log}, {
		start => $start_time,
		end => clock_gettime(CLOCK_MONOTONIC),
		url => $request->uri,
		code => $res->code,
		content => $request->content,
	};

	return $res;
}

sub clear_request_log {
	my $self = shift;
	$self->_set_request_log( [] );
}

sub credentials {
	my ($self, $host, $realm, $user, $pass) = @_;
	$self->_set_auth_host($host);
	$self->_set_auth_realm($realm);
	$self->_set_auth_user($user);
	$self->_set_auth_pass($pass);
}

1;
