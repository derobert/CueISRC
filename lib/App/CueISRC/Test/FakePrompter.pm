package App::CueISRC::Test::FakePrompter;
use Moo;
use Contextual::Return;
use 5.020;

has prompter => (
	is      => 'ro',
	default => sub {
		my $self = shift; return sub { $self->_real_prompter(@_) };
	},
);

has _exact  => ( is => 'ro', default => sub { return {} } );
has _regexp => ( is => 'ro', default => sub { return [] } );

sub add {
	my ($self, $prompt, $response) = @_;

	if ($prompt->isa('Regexp')) {
		push @{$self->_regexp}, [$prompt, $response];
	} else {
		exists ${$self->_exact}{$prompt}
			and die "Tried to add duplicate exact prompt question";
		${$self->_exact}{$prompt} = $response;
	}

	return;
}

my %io_prompter_args = (
	# ordered like in IO::Prompter docs
	-comp      => 1,
	-complete  => 1,
	-echo      => 1,
	-f         => 0,
	-filenames => 0,
	-stdio     => 0,

	# default is missing from the summary table. Note we don't handle
	# the short form.
	-DEF     => 1,
	-DEFAULT => 1,
	-def     => 1,
	-default => 1,
);

sub _real_prompter {
	my $self = shift;
	my $prompt;
	while (@_) {
		$prompt = $_[0];
		if (exists $io_prompter_args{$prompt}) {
			splice @_, 0, 1+$io_prompter_args{$prompt};
		} else {
			last;
		}
	}

	return (
		SCALAR { ${$self->_exact}{$prompt} }
		BOOL { 1 }
	) if exists ${$self->_exact}{$prompt};

	foreach my $re (@{$self->_regexp}) {
		return (
			SCALAR { $re->[1] }
			BOOL { 1 }
		) if $prompt =~ $re->[0];
	}

	die "Unexpected prompt: $prompt";
}

1;
