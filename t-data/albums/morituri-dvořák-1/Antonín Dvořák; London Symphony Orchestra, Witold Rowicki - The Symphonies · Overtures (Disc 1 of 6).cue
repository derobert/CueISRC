REM DISCID 5E10C006
REM COMMENT "Morituri 0.2.3"
CATALOG 0028947822974
FILE "00. Antonín Dvořák; London Symphony Orchestra, Witold Rowicki - Hidden Track One Audio.flac" WAVE
  TRACK 01 AUDIO
    ISRC NLA507000522
    INDEX 00 00:00:00
FILE "01. Antonín Dvořák - Symphony, No. 1 in C minor, Op. 3 - _The bells of zlonice_ - I. Maestoso - allegro.flac" WAVE
    INDEX 01 00:00:00
  TRACK 02 AUDIO
    ISRC NLA507000523
    INDEX 00 16:12:00
FILE "02. Antonín Dvořák - Symphony, No. 1 in C minor, Op. 3 - _The bells of zlonice_ - II. Adagio di molto.flac" WAVE
    INDEX 01 00:00:00
  TRACK 03 AUDIO
    ISRC NLA507000524
    INDEX 00 13:38:00
FILE "03. Antonín Dvořák - Symphony, No. 1 in C minor, Op. 3 - _The bells of zlonice_ - III. Allegretto.flac" WAVE
    INDEX 01 00:00:00
FILE "04. Antonín Dvořák - Symphony, No. 1 in C minor, Op. 3 - _The bells of zlonice_ - IV. Finale (Allegro animato).flac" WAVE
  TRACK 04 AUDIO
    ISRC NLA507000525
    INDEX 01 00:00:00
  TRACK 05 AUDIO
    ISRC NLA506700368
    INDEX 00 13:08:00
FILE "05. London Symphony Orchestra, Witold Rowicki - Carnival overture, Op. 92.flac" WAVE
    INDEX 01 00:00:00
  TRACK 06 AUDIO
    ISRC NLA507100510
    INDEX 00 08:53:00
FILE "06. Antonín Dvořák - Muj domov overture, Op. 62 (My country).flac" WAVE
    INDEX 01 00:00:00
