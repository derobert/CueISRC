{
	music       => {path     => 't-data/albums'},
	musicbrainz => {username => 'test', password => 'sekret'},
	worklog => {
		backend => {
			1 => {file => ':memory:', module => 'SQLite'},
		},
	},
};
